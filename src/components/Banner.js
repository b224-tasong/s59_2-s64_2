import { Row, Col, Button, Container } from 'react-bootstrap';
import Typewriter from 'typewriter-effect';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import { useContext } from 'react';


export default function Banner() {

	const {user} = useContext(UserContext);

	return(
			<Container className='my-5'>

						<Row>
							<Col>
									<h1>
									<Typewriter
									options={{
									  strings: ['Welcome to my Ecommerce Website','Where you can shop for stylish shoes.'],
									  autoStart: true,
									  loop: true,
									  delay: 5
									}}
									/>	
									
									</h1>
									<p>Take a step to an stylish opportunities.</p>
									{
										(user.id === null || user.isAdmin === false) ?
										
										<Button variant='primary' as={Link} to='/login'>Buy Now!</Button>
										:
										<>
										</>
									}
							</Col>
						</Row>
				
			</Container>
		)
}



