import { useState, useEffect } from 'react';
import { Row, Col, Card, Button, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function ProductCard({prodProp}) {

	// Checks to see if the data was successfully passed.
	// console.log(props)
	// Every component receives information in a form of object.
	// console.log(typeof props)
	// Using the dot notation, access the property to retrieve the value/data.
	// console.log(props.courseProp.name)
	// Checks if we can retrieve data from courseProp
	// console.log(courseProp)

	/*
		Use the state hook for this component to be able to store its state.
		States are used to keep track of the information related to individual components

		Syntax: 
			const [getter, setter] = useState(initialGetterValue)
	*/


	const { name, description, price, _id, imageLink } = prodProp;

	return (


		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight p-3">
					  <Card.Img variant="top" src={imageLink} />
				      <Card.Body>
				        <Card.Title>{name}</Card.Title>
				        <Card.Subtitle>Description:</Card.Subtitle>
				        <Card.Text>{description}</Card.Text>
				        <Card.Subtitle>Price:</Card.Subtitle>
				        <Card.Text>PhP {price}</Card.Text>
				        <Button className="bg-primary" as={Link} to={`/product/${_id}`} >purchase</Button>
				      </Card.Body>
				</Card>
			</Col>
		</Row>

	)
}