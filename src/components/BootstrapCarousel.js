import { Carousel, Container, Row, Col } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';


export default function BootstrapCarousel() {

		return(


				<Container className='mt-5 p-0 carousel-Container'>
					<Row>
						<Col lg={12}>
							<Carousel>
								<Carousel.Item interval={3000}>
							        <img
							          className="image-fluid carousel-img"
							          src='https://scontent.fdvo2-2.fna.fbcdn.net/v/t39.30808-6/325796639_485702320395595_1368200322296483954_n.jpg?_nc_cat=110&ccb=1-7&_nc_sid=0debeb&_nc_eui2=AeFx116zkoh62L_7U7t-gLGsxN_CW8gspJ3E38JbyCyknfWtm7-w0hYU8GfvUAfndUpNIqPvf21zfF_YeMnmCNiN&_nc_ohc=2OuL0Tc8kjUAX8J0L0n&_nc_oc=AQk2QNdsAydM_p02mo5OICp9zkDpQ6JbTtj08SagQxsb-JxKYvPP7GmtLTIe0Sm0Mnc&_nc_ht=scontent.fdvo2-2.fna&oh=00_AfA4xjkOLgEom7Vxf4wUbM0wjFlVjyEMB_WYSRG-BejTtA&oe=63CE65AD'
							          alt="First slide"
							        />
							    </Carousel.Item>
							    <Carousel.Item interval={3000}>
							        <img
							          className="image-fluid carousel-img"
							          src='https://scontent.fdvo2-1.fna.fbcdn.net/v/t39.30808-6/325500245_1182767369293644_5468740293728799092_n.jpg?_nc_cat=102&ccb=1-7&_nc_sid=0debeb&_nc_eui2=AeFl9tsW9PGwr2O4PdHp3I5AV23ha0ACgatXbeFrQAKBqzpc_OZXADvhrP4ucDUZERxjW4fhfHLwRDiyTMTB1UMW&_nc_ohc=NbITalPlVFIAX85CjyQ&_nc_ht=scontent.fdvo2-1.fna&oh=00_AfCgoZsFEYGAqrVXDIJAm2pzyocSzJWKBr_wlWpSJ2aoPQ&oe=63CF52FF'
							          alt="First slide"
							        />
							    </Carousel.Item>
							    <Carousel.Item interval={3000}>
							        <img
							          className="image-fluid carousel-img"
							          src='https://scontent.fdvo2-2.fna.fbcdn.net/v/t39.30808-6/325843856_495502556057176_2776371460412932246_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=0debeb&_nc_eui2=AeFQsAo-7lnV4CZnOP4yAmKL5dpPBIDGN73l2k8EgMY3vUhk5rUQz4yoE_h_l5dKGtHvgTKpCe0v8gB-f2Ik8KUn&_nc_ohc=uh0Mg_nSfyIAX86OCPm&_nc_ht=scontent.fdvo2-2.fna&oh=00_AfBXl7EuubLI3XKJWHUftqxpC3Y29uAqD2t1NseC-rGAjw&oe=63CF6201'
							          alt="First slide"
							        />
							    </Carousel.Item>
							     <Carousel.Item interval={3000}>
							        <img
							          className="image-fluid carousel-img"
							          src='https://scontent.fdvo2-2.fna.fbcdn.net/v/t39.30808-6/325340975_717163039976110_2878232076416737711_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=0debeb&_nc_eui2=AeHNUC2-A8mng2MdA8TI4y19H1s6taHjFBYfWzq1oeMUFkBUVtXyiKNGCZ4OYgKUadbdNpWDZInhXzjeIDPhQW5H&_nc_ohc=p1HeianwxqwAX8MkKOD&_nc_ht=scontent.fdvo2-2.fna&oh=00_AfBtLB41Fg8En3ARGnxepVS6gBDkPvR8RImt6FdHEcr2Yg&oe=63CF2F49'
							          alt="First slide"
							        />
							    </Carousel.Item>
							</Carousel>
						</Col>
					</Row>
				</Container>
			

			)
}