import { Row, Col, Card, Button, Container, Modal, Form } from 'react-bootstrap';
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2'


import '../css/style.css'


export default function Highlights() {

	const [allProduct, setAllProduct] = useState();
	const {user} = useContext(UserContext)
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [imageLink, setImageLink] = useState("");
	const [quantity, setQuantity] = useState("");
	const [productId, setProductId] = useState("");
	const [isActive, setIsActive] = useState(false);
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
 	const handleShow = () => setShow(true);
 	const navigate = useNavigate();



	function getProductInfo(id) {
		fetch(`${process.env.REACT_APP_API_URL}/products/singleProduct/${id}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setImageLink(data.imageLink)
			setProductId(data._id)

		})
	}


	const storeData = (id) => {
		handleShow();	
		getProductInfo(id);
		
	}


	function addToCart() {

		console.log(productId)
		console.log(quantity)
		fetch(`${process.env.REACT_APP_API_URL}/users/cartData`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				productId: productId,

			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data !== null) {

				Swal.fire({
					title: 'Saved to cart',
					icon: 'success',
					text: 'Item Added to Cart'
				})
				setQuantity("")
				navigate("/user")
				handleClose()
			}else{
				Swal.fire({
					title: 'Failed to add cart',
					icon: 'error',
					text: 'Try again'
				})
			}
		})
	}




	useEffect(() => {

			fetch(`${process.env.REACT_APP_API_URL}/products/`)
			.then(res => res.json())
			.then(data => {
				
				if (data !== null) {

					setAllProduct(data.map(product => {
						return (
							<Col lg={4} md={4} key={product._id}>
								<Card className="my-3 card-shadow">
										<Card.Img className='img-fluid'  variant="top" src={product.imageLink} />
										<Card.Body>
											<Card.Title className='title-fonts' style={{ height: '70px'}}>{product.name}</Card.Title>
											<Card.Subtitle style={{height: '40px', lineHeight: '40px'}}>Description:</Card.Subtitle>
											<Card.Text style={{height: '80px', lineHeight: '20px', fontSize: '14px'}}> {product.description}</Card.Text>
											<Card.Subtitle>Price:</Card.Subtitle>
											<Card.Text style={{fontSize: '14px', lineHeight: '30px'}}>PhP {product.price}</Card.Text>
											{
												(user.id === null) ?
												<>
												<Button className="bg-primary" as={Link} to={`/product/${product._id}`} >Purchase</Button>
												</>
												:
													(user.isAdmin === false) ?

														<>
														<Button className="bg-primary border-0 me-1" as={Link} to={`/product/${product._id}`} >Purchase</Button>
														<Button className="bg-danger border-0" onClick={() => storeData(product._id)}>Add to Cart</Button>
														</>
														:
														null
											}	
										</Card.Body>
								</Card>
							</Col>
							
						)
					}))
					
				}else{
					return null
				}
			})
		})




	useEffect(()=>{
		if (quantity !== "" && quantity >= 1) {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	},[quantity])




	return(
		<>
			<Modal show={show} onHide={handleClose}>
			       <Modal.Body>
			       		<Card className="cardHighlight p-3 border-0">
							<Card.Img variant="top" src={imageLink} />
							   <Card.Body>
							        <Card.Title className='title-fonts' style={{ height: '70px'}}>{name}</Card.Title>
							        <Card.Subtitle style={{height: '40px', lineHeight: '40px'}}>Description:</Card.Subtitle>
							        <Card.Text style={{height: '80px', lineHeight: '20px', fontSize: '14px'}}>{description}</Card.Text>
							        <Card.Subtitle>Price:</Card.Subtitle>
							        <Card.Text style={{fontSize: '14px', lineHeight: '30px'}}>PhP {price}</Card.Text>        
							        <Button className="bg-danger border-0" onClick={() => addToCart()} >Add to Cart</Button>
							 </Card.Body>
						</Card>
			       </Modal.Body>
			 </Modal>

			<Container className='my-3'>
				<Row>	
						{allProduct}
				</Row>	
			</Container>
		</>	

		)
}