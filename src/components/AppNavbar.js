// Old practice in importing components
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';

// Better practice in importing components
import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext'
import '../css/style.css'

import SS from '../SS.png';
import {FaShoppingCart} from 'react-icons/fa';


export default function AppNavbar() {

	const { user } = useContext(UserContext);
	return (

		<Navbar className='NavigationBarStyle' expand="lg">
				      <Container>
				       {
				       	(user.id === null || user.isAdmin === true) ?
				       	 <Navbar.Brand as={Link} to="/home" href="#home"><img src={SS} alt='logo' width="150" height="80"/></Navbar.Brand>
				       	:
				       	 <Navbar.Brand as={Link} to="/user" href="#user"><img src={SS} alt='logo' width="150" height="80"/></Navbar.Brand>
				       }
				        <Navbar.Toggle aria-controls="basic-navbar-nav" />
				        <Navbar.Collapse id="basic-navbar-nav">
				          <Nav className="ms-auto">
				            
				          	{
				          		(user.id === null) ?
				          		<>
				          		<Nav.Link as={Link} to="/home" href="#home" className='header-fonts me-4'>Home</Nav.Link>
				          		<Nav.Link as={Link} to="/register" href="#register" className='header-fonts me-4'>Register</Nav.Link>
				          		<Nav.Link as={Link} to="/login" href="#login" className='header-fonts'>Login</Nav.Link>
				          		</>
				          		:			            	
					            	(user.isAdmin) ?

					            		<>
					            		<Nav.Link as={Link} to="/home" href="#home" className='header-fonts me-4'>Home</Nav.Link>
					            		<Nav.Link as={Link} to="/dashboard" href="#dashboard" className='header-fonts me-4'>Dashboard</Nav.Link>
					            		<Nav.Link as={Link} to="/userinfo" href="#user" className='header-fonts me-4'>User</Nav.Link>
						            	<Nav.Link as={Link} to="/logout" href="#logout" className='header-fonts'>Logout</Nav.Link>
						            	</>
					            		:
					            		<>
					            		<Nav.Link as={Link} to="/user" href="#home" className='header-fonts me-4'>Home</Nav.Link>
					            		<Nav.Link as={Link} to="/logout" href="#logout" className='header-fonts me-4'>Logout</Nav.Link>
					            		<Nav.Link as={Link} to="/cart" href="#cart" className='header-fonts'><FaShoppingCart size={30}/></Nav.Link>
					            		</>
					            

				            }	
				          </Nav>
				        </Navbar.Collapse>
				      </Container>
		</Navbar>


		)
}