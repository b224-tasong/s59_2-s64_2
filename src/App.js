// import { Fragment } from 'react';
import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css'

import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import ProductView from './pages/ProductView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Dashboard from './pages/Dashboard';
import AddProductForm from './pages/AddProductForm';
import UpdateProductForm from './pages/UpdateProductForm';
import UserInfo from './pages/UserInfo';
import UpdateUserInfo from './pages/UpdateUserInfo';
import User from './pages/User';
import Cart from './pages/Cart';

import './App.css';
import { UserProvider } from './UserContext'

function App() {

    const [user, setUser] = useState({
        
        id: null,
        isAdmin: null

    });

    const unsetUser = () => {
        localStorage.clear();
    }

    useEffect(() =>{
        fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            if (typeof data._id !== "undefined") {

                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })

            }else{
                setUser({
                    id: null,
                    isAdmin: null
                })
            }
        })
    }, [])


  return (

    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
            <AppNavbar />
            <Container>
                <Routes>

                    <Route path='*' element={<Error/>} />
                    <Route path="/" element={<Home/>} />
                    <Route path="/home" element={<Home/>} />
                    <Route path="dashboard" element={<Dashboard/>} />
                    <Route path="dashboard/:product._id" element={<Dashboard/>} />
                    <Route path="addproductform" element={<AddProductForm/>} />
                    <Route path="updateproductform" element={<UpdateProductForm/>} />
                    <Route path="/product/:prodId" element={<ProductView/>}/>
                    <Route path="/updateproductform/:prodId" element={<UpdateProductForm/>}/>
                    <Route path="/register" element={<Register/> }/>
                    <Route path="/userinfo" element={<UserInfo/> }/>
                    <Route path="/userinfo/:userDetails._id" element={<UserInfo/> }/>
                    <Route path="/updateuserinfo/:userId" element={<UpdateUserInfo/> }/>
                    <Route path="user" element={<User/>}/>
                    <Route path="/login" element={<Login/>}/>
                    <Route path="/cart" element={<Cart/>}/>
                    <Route path="/cart/:productId" element={<Cart/>}/>
                    <Route path="/logout" element={<Logout/>}/>

                </Routes>
            </Container>
        </Router>
    </UserProvider>
  );
}

export default App;
