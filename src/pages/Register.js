import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';
import { Navigate, useNavigate } from 'react-router-dom';



export default function Register() {

	const { user } = useContext(UserContext);
	const nav = useNavigate();
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	// Satet to determine wether the submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);



	// To simulate user registration function

	function registerUser(e) {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail/`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {

				Swal.fire({
					title: 'Dulicate Email Found',
					icon: 'error',
					text: 'Please try a different email'
				})
				setEmail('');

			}else{
	
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},

					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo

					})
				})
				.then(res => res.json())
				.then(data => {

					if (data === true) {

						// Clearing the input fields and states
						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword1('');
						setPassword2('');
						setMobileNo('');

						Swal.fire({
							title: 'You have successfully registered',
							icon: 'success',
							text: 'Congratulation!'
						})

						nav("/login")
						
						
					}else{
						Swal.fire({
							title: 'Failed to register account',
							icon: 'success',
							text: 'Please try again!'
						})
					}
				})
			}
		})	
	}

	useEffect(()=>{
		if ((firstName !== "" && lastName !== "" && email !== "" && password1 !== "" && password2 !== "" && mobileNo !== "") && (password1 === password2)) {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [firstName, lastName, email, password1, password2, mobileNo]);


	return(
			
			(user.id !== null) ?
			<Navigate to="/home" />
			:
			<Form className='mt-5' onSubmit={e => registerUser(e)}>
				 <Form.Group className="mb-3" controlId="firstName">
				   <Form.Label>First Name</Form.Label>
				   <Form.Control type="text"
				   	 	value={firstName}
				   	 	onChange={e => setFirstName(e.target.value)}
				   	 	required
				    />
				 </Form.Group>

			     <Form.Group className="mb-3" controlId="lastName">
			       <Form.Label>Last Name</Form.Label>
			       <Form.Control type="text" 	
			       	 	value={lastName}
			       	 	onChange={e => setLastName(e.target.value)}
			       	 	required
			        />
			     </Form.Group>

			     <Form.Group className="mb-3" id ='email' controlId="email">
			       <Form.Label>Email</Form.Label>
				       <Form.Control 
					       type="email" 
					       value={email}
					       onChange={e => setEmail(e.target.value)}
					       required
				       />
			   	  </Form.Group>

			   	  <Form.Group className="mb-3" controlId="mobileNo">
			       <Form.Label>Mobile Number</Form.Label>
				       <Form.Control 
					       type="tel" 
					       value={mobileNo}
					       onChange={e => setMobileNo(e.target.value)}
					       maxLength={11}
					       required
				       />
			   	  </Form.Group>

			     <Form.Group className="mb-3" controlId="password1">
			       <Form.Label>Password</Form.Label>
				       <Form.Control 
					       type="password" 
					       value={password1}
					       onChange={e => setPassword1(e.target.value)}
					       required
				       />
			   	  </Form.Group>

			   	  <Form.Group className="mb-3" controlId="password2">
			       <Form.Label>Verify Password</Form.Label>
				       <Form.Control 
					       type="password" 
					       value={password2}
					       onChange={e => setPassword2(e.target.value)}
					       required
				       />
			   	  </Form.Group>

			     {
			     	isActive ? 
			     		<Button variant="primary" type="submit" id="submitBbtn">
			     		  Submit
			     		</Button>
			     		:
			     		<Button variant="primary" type="submit" disabled>
			     		  Submit
			     		</Button>
			     }
			</Form>
		)
}