import { useEffect, useState } from 'react';
import ProductCard from '../components/ProductCard';
import { Container, Row, Col } from 'react-bootstrap';
// import coursesData from '../data/coursesData';

export default function Product() {

	const [product, setProduct] = useState([]);

	// Checks to see if the mock data was captured.
	// console.log(coursesData);
	// console.log(coursesData[0]);


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(res => res.json())
		.then(data => {
			
			setProduct(data.map(product => {
				return (

					<ProductCard/>
				)
			}))
		})
	}, [])


	return (
		<>
		<Container>
					{product}
		</Container>
		</>
	)
}
