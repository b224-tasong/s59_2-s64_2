import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function Banner() {
	return(

			<Row>
				<Col>
					<h1>Error 404 - Page Not Found</h1>
					<p>This page can't be found!</p>
					<Button variant='primary' as={Link} to="/">Back to Home</Button>
				</Col>
			</Row>
		)
}