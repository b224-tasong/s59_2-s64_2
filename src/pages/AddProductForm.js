import { Form, Button } from 'react-bootstrap';
import { useState, useEffect,useContext } from 'react';
import Swal from 'sweetalert2';
import { useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';



			
export default function AddProductForm() {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [imageLink, setImageLink] = useState('');
	const [price, setPrice] = useState('');
	const [isActive, setIsActive] = useState(false);
	const nav = useNavigate();
	const { user } = useContext(UserContext);


	function createProduct(e) {
		console.log(user.isAdmin)
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				name: name,
				description: description,
				imageLink: imageLink,
				price: price
			})
			
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {

				Swal.fire({
				title: 'Product Saved',
				icon: 'success',
				text: 'You have successfully saved your product'
				})
				nav("/dashboard")
			}else{
				Swal.fire({
				title: 'Failed to save product',
				icon: 'error',
				text: 'Please try a again'
				})
			}

		})
	}

	useEffect(()=>{
		if (name !== "" && description !== "" && price !== "" && imageLink !== "") {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [name, description, price, imageLink]);


	return(

			<Form className='mt-5' onSubmit={e => createProduct(e)}>
				 <Form.Group className="mb-3" controlId="name">
				   <Form.Label>Product Name</Form.Label>
				   <Form.Control type="text"
				   	 	value={name}
				   	 	onChange={e => setName(e.target.value)}
				   	 	required
				    />
				 </Form.Group>

			     <Form.Group className="mb-3" controlId="description">
			       <Form.Label>Product Description</Form.Label>
			       <Form.Control type="text" 	
			       	 	value={description}
			       	 	onChange={e => setDescription(e.target.value)}
			       	 	required
			        />
			     </Form.Group>

			     <Form.Group className="mb-3" id ='email' controlId="imageLink">
			       <Form.Label>Image Link</Form.Label>
				       <Form.Control 
					       type="text" 
					       value={imageLink}
					       onChange={e => setImageLink(e.target.value)}
					       required
				       />
			   	  </Form.Group>

			   	  <Form.Group className="mb-3" controlId="price">
			       <Form.Label>Price</Form.Label>
				       <Form.Control 
					       type="number" 
					       value={price}
					       onChange={e => setPrice(e.target.value)}
					       maxLength={6}
					       required
				       />
			   	  </Form.Group>

			     {
			     	isActive ? 
			     		<Button variant="primary" type="submit" id="submitBbtn">
			     		  Submit
			     		</Button>
			     		:
			     		<Button variant="primary" type="submit" disabled>
			     		  Submit
			     		</Button>
			     }
			</Form>

		)
}