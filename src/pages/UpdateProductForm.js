import { Form, Button } from 'react-bootstrap';
import { useState, useEffect,useContext } from 'react';
import Swal from 'sweetalert2';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function UpdateProductForm() {

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [imageLink, setImageLink] = useState('');
	const [price, setPrice] = useState(''); 
	const nav = useNavigate();
	const { prodId } = useParams();
	const { user } = useContext(UserContext);


	


	function updateProduct(e) {
		console.log(prodId)
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/products/${prodId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				name: name,
				description: description,
				imageLink: imageLink,
				price: price
			})
			
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if (data === true) {

				Swal.fire({
				title: 'Product Deatails',
				icon: 'success',
				text: 'Updates successfully'
				})
				nav("/dashboard")
			}else{
				Swal.fire({
				title: 'Update Failed',
				icon: 'error',
				text: 'Please try again'
				})
			}

		})
	}

	useEffect(() => {

		console.log(prodId)

		fetch(`${process.env.REACT_APP_API_URL}/products/singleProduct/${prodId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImageLink(data.imageLink);


		})


	}, [prodId])

	return(

			(user.isAdmin) ?
			<Form className='mt-5' onSubmit={e => updateProduct(e)}>
				 <Form.Group className="mb-3" controlId="name">
				   <Form.Label>Product Name</Form.Label>
				   <Form.Control type="text"
				   	 	value={name}
				   	 	onChange={e => setName(e.target.value)}
				   	 	required
				    />
				 </Form.Group>

			     <Form.Group className="mb-3" controlId="description">
			       <Form.Label>Product Description</Form.Label>
			       <Form.Control type="text" 	
			       	 	value={description}
			       	 	onChange={e => setDescription(e.target.value)}
			       	 	required
			        />
			     </Form.Group>

			     <Form.Group className="mb-3" id ='email' controlId="imageLink">
			       <Form.Label>Image Link</Form.Label>
				       <Form.Control 
					       type="text" 
					       value={imageLink}
					       onChange={e => setImageLink(e.target.value)}
					       required
				       />
			   	  </Form.Group>

			   	  <Form.Group className="mb-3" controlId="price">
			       <Form.Label>Price</Form.Label>
				       <Form.Control 
					       type="number" 
					       value={price}
					       onChange={e => setPrice(e.target.value)}
					       maxLength={6}
					       required
				       />
			   	  </Form.Group>
			      <Button variant="primary" type="submit" id="submitBbtn">
			     		  Submit
			      </Button>		     		
			</Form>
			:
			<Navigate to='/home/'/>
		)


}