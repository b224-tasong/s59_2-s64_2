import { Form, Button } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import { useState, useEffect, useContext} from 'react';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';


export default function Login() {


	// This one allows as to consum the user context object and its properties to be used for validation.
	const { user, setUser } = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);
	const navigate = useNavigate();



	function loginUser(e) {
		
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data =>{
			// We will receive either a token or a false response.

			if (typeof data.tokenAccess !== "undefined") {
				localStorage.setItem('token', data.tokenAccess )
				retrieveUserDetails(data.tokenAccess)

				fetch(`${process.env.REACT_APP_API_URL}/users/checkAdmin`,{
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email: email
					})
				})
				.then(res => res.json())
				.then(data => {
					
					if (data.isAdmin === true) {
						Swal.fire({
							title: 'Login Successfully',
							icon: 'success',
							text: 'Welcome Admin'
						})
						navigate("/home")
					}

					if (data.isAdmin === false) {
						Swal.fire({
							title: 'Login Successfully',
							icon: 'success',
							text: 'Welcome Admin'
						})
						navigate("/user")
					}
				})

				
			}else{
				Swal.fire({
					title: 'Authentication Failed!',
					icon: 'error',
					text: 'Incorrect Email or Password!'

				})
			}

			
		})


	
	}


	const retrieveUserDetails = (token) =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
			headers: {
				Authorization: `Bearer ${token}`
			}

		})
		.then(res => res.json())
		.then(data=> {

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(()=>{
		if (email !== "" && password !== "") {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password]);


	return(

			(user.id !== null) ?
				<Navigate to="/home" />
				:
				<Form className="mt-5" onSubmit={e => loginUser(e)}>
				      <Form.Group className="mb-3" controlId="userEmail">
				      		<Form.Label>Email address</Form.Label>
				      		<Form.Control type="email"
				      			placeholder="Enter email" 
				      			value={email}
				      			onChange={e => setEmail(e.target.value)}
				      			required
				      		/>
				      </Form.Group>

				       <Form.Group className="mb-3" controlId="password">
				      		<Form.Label>Password</Form.Label>
				      		<Form.Control 
				      			type="password" 
				      			placeholder="Password" 
				      			value={password}
				      			onChange={e => setPassword(e.target.value)}
				      			required
				      		/>
				      </Form.Group>
				      {
				      	isActive ?
				      	<Button variant="success" type="submit" id="submitBbtn">
				      	  Submit
				      	</Button>
				      	:
				      	<Button variant="success" type="submit" disabled>
				      	  Submit
				      	</Button>
				      }
				</Form>

		)

}