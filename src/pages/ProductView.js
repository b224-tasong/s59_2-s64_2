import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2'
import UserContext from '../UserContext';
import '../css/style.css';

export default function ProductView() {

	const { user } = useContext(UserContext);

	// This one allows us to gain access to method that will allow us to redirect a user
	// different page after enrolling to a course
	const navigate= useNavigate();

	// The "useParams" hook allows us to retrieve the courseId passed via the URL params.
	const { prodId } = useParams();


	
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [imageLink, setImageLink] = useState("");
	const [productId, setProductId] = useState("");
	const [quantity, setQuantity] = useState("");

	const purchase = (e, productId, quantity) => {
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/order`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},

			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			

			if (data === true) {
				Swal.fire({
					title: 'Product has been purchased successfuly',
					icon: 'success',
					text: 'Thank you for puchasing in our store'
				})

				navigate("/user")

			}else{
				Swal.fire({
					title: 'Failed to Purchase',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	};

	useEffect(() => {

		

		fetch(`${process.env.REACT_APP_API_URL}/products/singleProduct/${prodId}`)
		.then(res => res.json())
		.then(data => {
			

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setImageLink(data.imageLink);
			setProductId(data._id);


		})


	}, [prodId])


	return (

		<Container className='my-5'>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
						  <Card.Img variant="top" src={imageLink} />
					      <Card.Body>
					        <Card.Title className='mb-3'>{name}</Card.Title>
					        <Card.Subtitle>Description:</Card.Subtitle>
					        <Card.Text className='font-1'>{description}</Card.Text>
					        <Card.Subtitle>Price:</Card.Subtitle>
					        <Card.Text>PhP {price}</Card.Text>
					         
						        <Form.Group className="mb-3 w-25" controlId="formGroupEmail">
						                <Form.Label>Quantity</Form.Label>
						                <Form.Control type="number"
						                	value={quantity}
						                	onChange={e => setQuantity(e.target.value)}
						                />
						        </Form.Group>
					         
					        {
					        	(user.id !== null || user.isAdmin === false) ?
					        	<Button variant="primary" onClick={(e) => purchase(e,productId, quantity)}>Purchase</Button>
					        	:
					        	<Button className='btn btn-danger' as={Link} to='/login'>Login to Purchase</Button>
					        }
					      </Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>

	)
}