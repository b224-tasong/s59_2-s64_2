import {useContext, useEffect} from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout() {

	// localStorage.clear();

	const { unsetUser, setUser } = useContext(UserContext);

	unsetUser();

	useEffect(()=> {
		
		setUser({id: null})

	}, [setUser])

	return(
		// This one redirects user back to the login page
		<Navigate to="/home"/>


	)
}