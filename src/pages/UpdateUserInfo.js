import { Form, Button, Row, Col, Container } from 'react-bootstrap';
import { useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function UpdateUserInfo() {

	const { userId } = useParams();
	const { user } = useContext(UserContext);
	const nav = useNavigate();
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');



	function updateUser(e) {

		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/users/updateUserInfo/${userId}`,{
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({

				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo

			})
		})
		.then(data => data.json())
		.then(data => {

			if (data === true) {

				Swal.fire({
				title: 'User Details',
				icon: 'success',
				text: 'Updates successfully'
				})
				nav("/userinfo")
			}else{
				Swal.fire({
					title: 'Update Failed',
					icon: 'error',
					text: 'Please try again'
				})
			}
		})
	}
	

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/users/userDetails/${userId}`)
		.then(res => res.json())
		.then(data => {
			

			setFirstName(data.firstName)
			setLastName(data.lastName)
			setEmail(data.email)
			setMobileNo(data.mobileNo)


		})


	}, [userId])

	return(

				(user.id !== null && user.isAdmin === true) ?
				<Container>
					<Row md={2}>
						<Col>
							<Form className='mt-5' onSubmit={e => updateUser(e)}>
								 <Form.Group className="mb-3" controlId="firstName">
								   <Form.Label>First Name</Form.Label>
								   <Form.Control type="text"
								   	 	value={firstName}
								   	 	onChange={e => setFirstName(e.target.value)}
								   	 	required
								    />
								 </Form.Group>

							     <Form.Group className="mb-3" controlId="lastName">
							       <Form.Label>Last Name</Form.Label>
							       <Form.Control type="text" 	
							       	 	value={lastName}
							       	 	onChange={e => setLastName(e.target.value)}
							       	 	required
							        />
							     </Form.Group>

							     <Form.Group className="mb-3" id ='email' controlId="email">
							       <Form.Label>Email</Form.Label>
								       <Form.Control 
									       type="email" 
									       value={email}
									       onChange={e => setEmail(e.target.value)}
									       required
								       />
							   	  </Form.Group>

							   	  <Form.Group className="mb-3" controlId="mobileNo">
							       <Form.Label>Mobile Number</Form.Label>
								       <Form.Control 
									       type="tel" 
									       value={mobileNo}
									       onChange={e => setMobileNo(e.target.value)}
									       maxLength={11}
									       required
								       />
							   	  </Form.Group>

							      <Button variant="primary" type="submit" id="submitBbtn">
							     		  Submit
							      </Button>
							</Form>
						</Col>
					</Row>
				</Container>
				:
				<Navigate to="/" />	
		)

}