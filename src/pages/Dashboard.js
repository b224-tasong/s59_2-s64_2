import { Container, Table, Button  } from 'react-bootstrap'
import { useEffect, useState, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, Link,useNavigate } from 'react-router-dom';

import {FaFolderPlus} from 'react-icons/fa';
import Swal from 'sweetalert2';
// import DashboardTable from '../components/DashboardTable';


export default function Dashboard() {



	const [allProduct, setAllProduct] = useState([]);
	const { user } = useContext(UserContext);
	const nav = useNavigate();


	const fetchData = () =>{

		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`,{
			headers: {	
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			
			setAllProduct(data.map(product => {
				return (
				
					<tr key={product._id}>

						<td className='text-center align-middle'>{product.name}</td>
						<td className='text-center align-middle'>{product.description}</td>
						<td className='text-center align-middle'>{product.price}</td>
						<td className='text-center align-middle'>{product.isActive ? "Available" : "Not Available"}</td>
						<td className='text-center align-middle'><img src={product.imageLink} alt='products' style={{maxWidth: '100px'}}/></td>
						<td className='text-center align-middle'>
				     		{	
				     			(product.isActive) ?

				     			<Button className='btn bg-secondary border-0 text-center my-2' variant="primary" type="submit" id="submitBbtn"  style={{width: '100px', height: '35px', fontSize: '13px',  paddingLeft: '20px', paddingRight: '20px'}} onClick={() => archiveProduct(product._id)}>
						     		 Deactivate
						     	</Button>
				     			:
				     			<Button className='btn bg-danger border-0 text-center my-2' variant="primary" type="submit" id="submitBbtn"  style={{width: '100px', height: '35px', fontSize: '13px',  paddingLeft: '20px', paddingRight: '20px'}} onClick={() => activateProduct(product._id)}>
				     				 Activate
				     			</Button>
				     		}	
				     		<Button className='btn bg-success border-0 text-center my-2' variant="primary" type="submit" id="submitBbtn"  style={{width: '100px', height: '35px', fontSize: '13px',  paddingLeft: '20px', paddingRight: '20px'}}  as={Link} to={`/updateproductform/${product._id}`}>
				     		 Update
				     		</Button>
				     		<Button className='btn bg-primary border-0 text-center my-2' variant="primary" type="submit" id="submitBbtn"  style={{width: '100px', height: '35px', fontSize: '13px',  paddingLeft: '20px', paddingRight: '20px'}}  as={Link} to={`/updateproductform/${product._id}`}>
				     		 Purchases
				     		</Button>
				     	</td>
					</tr>	
				)
			}))
		})

	}

	const archiveProduct = (id) => {
		
		fetch(`${process.env.REACT_APP_API_URL}/products/archive/${id}`,{
			method: 'PATCH', 
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			
			if (data === true) {

				Swal.fire({
				title: 'Product Deactivated',
				icon: 'success',
				text: 'You have successfully deactivated the product'
				})
				nav("/dashboard")
			}else{
				Swal.fire({
				title: 'Failed to deactivate product',
				icon: 'error',
				text: 'Please try a again'
				})
			}

		})
	}



	const activateProduct = (id) => {
		

		fetch(`${process.env.REACT_APP_API_URL}/products/active/${id}`,{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			
			if (data === true) {

				Swal.fire({
				title: 'Product Activated',
				icon: 'success',
				text: 'You have successfully activated your product'
				})
				nav("/dashboard")
			}else{
				Swal.fire({
				title: 'Failed to activate product',
				icon: 'error',
				text: 'Please try a again'
				})
			}

		})
	}
	

	useEffect(() => {

			fetchData();

		})
	


	return(

		
		(user.id !== null && user.isAdmin === true) ?
		<Container className='mt-5'>
			<Button className='btn bg-primary border-0  text-center my-2' as={Link} to="/addproductform" variant="primary" type="submit" id="submitBbtn"  style={{width: '150px', height: '35px', fontSize: '13px',  paddingLeft: '20px', paddingRight: '20px'}}>
			     		 Add Product <FaFolderPlus size={20}/>
			</Button>	
			<Table striped bordered hover size="sm" className='table-dark'>
				<thead className='table-dark'>
					<tr>
						<th className='text-center align-middle' style={{width: '170px', height: '60px'}}>Product Name</th>
						<th className='text-center align-middle' style={{width: '600px'}}>Product decription</th>
						<th className='text-center align-middle' style={{width: '90px'}}>Price</th>
						<th className='text-center align-middle'>Product status</th>
						<th className='text-center align-middle' style={{width: '140px'}}>Product Image</th>
						<th className='text-center align-middle' style={{width: '120px'}}>Action</th>
					</tr>
				</thead>
				<tbody>
					{allProduct}   	    
				</tbody>
			</Table> 
		</Container>
			:
		<Navigate to="/home" />

		)
}