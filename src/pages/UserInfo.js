
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useEffect, useState, useContext } from 'react';
import { Navigate, Link, useNavigate } from 'react-router-dom';
import { Container, Table, Button, Modal, Form } from 'react-bootstrap';

export default function UserInfo() {

	const { user } = useContext(UserContext);
	const [userDetails, setUserDetails] = useState();
	const nav = useNavigate();
	const [currentPassword, setCurrentPassword] = useState('');
	const [newPassword, setNewPassword] = useState('');
	const [verifyPassword, setVerifyPassword] = useState('');
	const [storeId, setStoreId] = useState(null);
	
	const [isActive, setIsActive] = useState(false);
	const [show, setShow] = useState(false);
	const handleClose = () => setShow(false);
 	const handleShow = () => setShow(true);

 	


	function fetchData() {
		fetch(`${process.env.REACT_APP_API_URL}/users/allDetails`, {
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data =>{
			setUserDetails(data.map(userDetails =>{
				return(
						<tr key={userDetails._id} className='table-dark'>

							<td className='text-center align-middle'>{userDetails.firstName}</td>
							<td className='text-center align-middle'>{userDetails.lastName}</td>
							<td className='text-center align-middle'>{userDetails.email}</td>
							<td className='text-center align-middle'>{userDetails.mobileNo}</td>				
							<td className='text-center align-middle'>{userDetails.isAdmin ? "Admin": "User"}</td>				
							<td className='text-center align-middle'>
								<Button style={{width: '90px', marginBottom: '10px', fontSize: '13px'}} as={Link} to={`/updateuserinfo/${userDetails._id}`}>
									Update
								</Button>
								<Button style={{width: '90px', marginBottom: '10px', fontSize: '13px'}} onClick={() => storeData(userDetails._id)}>
									Password
								</Button>
								{
									(userDetails.isAdmin === true) ?
									<Button className='btn-success' style={{width: '90px', fontSize: '13px'}} onClick={() => setDataAsUser(userDetails._id)}>
										{
											userDetails.isAdmin ? "Demote" : "Promote"
										}
									</Button>
									:
									<Button className='btn-secondary' style={{width: '90px', fontSize: '13px'}} onClick={() => setDataAsAdmin(userDetails._id)}>
										{
											userDetails.isAdmin ? "Demote" : "Promote"
										}
									</Button>
								}
							</td>				
						
						</tr>
					)
			}))
		})
	}


	function setDataAsAdmin(id) {

		fetch(`${process.env.REACT_APP_API_URL}/users/setAdmin/${id}`,{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {

				Swal.fire({
				title: 'Role has been updated',
				icon: 'success',
				text: 'User is now an Admin'
				})
				nav("/userinfo")

			}else{
				Swal.fire({
				title: 'Failed to update Role',
				icon: 'success',
				text: 'Please try a again'
				})
			}
		})
	}


	function setDataAsUser(id) {

		fetch(`${process.env.REACT_APP_API_URL}/users/setUser/${id}`,{
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {
			if (data === true) {

				Swal.fire({
				title: 'Role has been updated',
				icon: 'success',
				text: 'Role has reverted to User'
				})
				nav("/userinfo")

			}else{
				Swal.fire({
				title: 'Failed to update Role',
				icon: 'success',
				text: 'Please try a again'
				})
			}
		})
	}


	const storeData = (id) => {
		handleShow();	
		setStoreId(id)
	}


	function savePassword (e){
			
			e.preventDefault()
			fetch(`${process.env.REACT_APP_API_URL}/users/comparePassword/${storeId}`,{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${localStorage.getItem('token')}`
				},

				body: JSON.stringify({

					password: currentPassword,
				
				})
			})
			.then(res => res.json())
			.then(data => {
				
				if (data === true) {
					
					fetch(`${process.env.REACT_APP_API_URL}/users/password/${storeId}`,{
						method: 'PATCH',
						headers: {
							'Content-Type': 'application/json',
							Authorization: `Bearer ${localStorage.getItem('token')}`
						},

						body: JSON.stringify({
							password1: currentPassword,
							password: newPassword
						})

					})
					.then(res => res.json())
					.then(data => {
						
						if (data === true) {

							Swal.fire({
							title: 'Successfully Saved',
							icon: 'success',
							text: 'Password has been changed'
							})
							setCurrentPassword("")
							setNewPassword("")
							setVerifyPassword("")
							handleClose()

						}else{

							Swal.fire({
							title: 'Failed to Chnage Password',
							icon: 'error',
							text: 'Please try a again'
							})
							setNewPassword("")
							setVerifyPassword("")
						}
					})

				}else{
					Swal.fire({
					title: 'Failed to Change Password',
					icon: 'error',
					text: 'Please try a again'
					})

					setCurrentPassword("")
				}
			})

	}


	function modalFieldsClear(){
		setCurrentPassword("")
		setNewPassword("")
		setVerifyPassword("")
	}


	useEffect(()=>{

		fetchData();
		
	})

	useEffect(()=>{
		if ((currentPassword !== "" && newPassword !== "" && verifyPassword !== "") && (newPassword === verifyPassword)) {
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [currentPassword, newPassword, verifyPassword]);

	return(
			
	
			
			<>
			<Modal show={show} onHide={handleClose}>
			       <Modal.Header closeButton>
			         <Modal.Title>Change Password</Modal.Title>
			       </Modal.Header>
			       <Modal.Body>
			       	
			       	<Form onSubmit={e => savePassword(e)}>
			       	      <Form.Group className="mb-3" controlId="currentPassword">
			       	        <Form.Label>Current Password</Form.Label>
			       	        <Form.Control type="password"
			       	        	value={currentPassword}
			       	        	onChange={e => setCurrentPassword(e.target.value)}
			       	         required/>
			       	      </Form.Group>
			       	      <Form.Group className="mb-3" controlId="newPassword">
			       	        <Form.Label>New Password</Form.Label>
			       	        <Form.Control type="password" 
			       	        	value={newPassword}
			       	        	onChange={e => setNewPassword(e.target.value)}
			       	        required/>
			       	      </Form.Group>
			       	      <Form.Group className="mb-3" controlId="verifyPassword">
			       	        <Form.Label>Verify Password Password</Form.Label>
			       	        <Form.Control type="password" required
			       	        	value={verifyPassword}
			       	        	onChange={e => setVerifyPassword(e.target.value)}
			       	        />
			       	      </Form.Group>
			       	      {
			       	      	isActive ?	
			       	      	<>
				       	      	<Button variant="primary" type="submit" className='me-2 border-0'>
				       	        Submit
				       	      	</Button>
				       	      	<Button variant="danger" type="submit" onClick={modalFieldsClear} className='me-2 border-0'>
				       	      	Clear	
				       	      	</Button>
				       	    </>
			       	      	:
			       	      	<>
				       	      	<Button variant="primary" type="submit" disabled className='me-2 border-0' >
				       	        Submit	
				       	        </Button>
				       	        <Button variant="danger" type="submit" onClick={modalFieldsClear} className='me-2 border-0'>
				       	        Clear	
				       	        </Button>
				       	    </>    
			       	      }
			       	</Form>

			       </Modal.Body>
			 </Modal>

			{
				 (user.isAdmin) ?
				<Container className='mt-5'>
					<Table striped bordered hover size="sm">
						<thead>
							<tr className='table-dark'>
								<th className='text-center align-middle' style={{width: '150px', height: '60px'}}>First Name</th>
								<th className='text-center align-middle' style={{width: '500px'}}>Last Name</th>
								<th className='text-center align-middle' style={{width: '90px'}}>Email</th>
								<th className='text-center align-middle' style={{width: '160px'}}>Mobile Number</th>
								<th className='text-center align-middle' style={{width: '120px'}}>Role</th>
								<th className='text-center align-middle' style={{width: '160px'}}>Action</th>
							</tr>
						</thead>
						<tbody>
							  {userDetails}  
						</tbody>
					</Table> 
				</Container>
				:
				<Navigate to="/dashboard" />
				
			}
		
			</>
		)

}