import {useEffect, useState, useContext } from 'react';
import { Navigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';


import { Table, Container, Form, Button } from 'react-bootstrap';
import '../css/style.css'

export default function Cart() {

	const [products, setProducts] = useState('');
	const [quantity, setQuantity] = useState(0);
	const { user } = useContext(UserContext);
	const [isActive, setIsActive] = useState(false);

	console.log(quantity)
	useEffect(()=> {

		fetch(`${process.env.REACT_APP_API_URL}/users/getCartDetails`,{
			method: 'GET',
			headers:{
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data =>{
 
			   if (data !== null) {

			 	  	setProducts(data.map(productData =>{
			   		return(
			   			<tbody key={productData._id}>
			   				<tr>
			   					<th className='ps-3 text-left align-middle' style={{width: '170px', height: '60px'}}>{productData.name}</th>
			   					<th className='text-center align-middle' style={{width: '90px'}}>{productData.price}</th>
			   					<th className='text-center align-middle'>
			   						 <div className="inputForm-Table">
			   						  {
			   						  	(isActive === true)?
			   						  	 <Button variant="danger" type="submit" className='inputFormButton' onClick={(e) => subtractQuantity(e)}>
									       -
									  	 </Button>
									  	 :
									  	  <Button variant="danger" type="submit" className='inputFormButton' disabled>
									       -
									  	  </Button>
			   						  }
								       <input type='text' className='inputForm' value={quantity} />
								        
								  	   <Button variant="danger" type="submit" className='inputFormButton' onClick={(e) => addQuantity(e)}>
			   							  +
			   						   </Button>
								     </div>
			   					</th>
			   					<th className='text-center align-middle'><img src={productData.imageLink} alt='products' style={{maxWidth: '100px'}}/></th>
			   					<th className='text-center align-middle'></th>
			   					<th className='text-center align-middle'></th>
			   				</tr>
			   			</tbody>
			   			)
				   	}))

			   }else{

			   	return null
			   }
			
		})

	},[]);

	useEffect(() => {
		if (quantity !== "" && quantity > 0) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [quantity]);


	function addQuantity(e){
		e.preventDefault()
		setQuantity(quantity + 1)
	}

	function subtractQuantity(e){
		e.preventDefault()
		if(quantity > 0){
			setQuantity(quantity - 1)
		}
	}

	return(

		(user.id !== null && user.isAdmin !== true) ?
		<Container className='mt-5'>
			
			<Table striped bordered hover size="sm" className='table-dark'>
				<thead className='table-dark'>
					<tr>
						<th className='text-center align-middle' style={{width: '170px', height: '60px'}}>Name</th>
						<th className='text-center align-middle' style={{width: '90px'}}>Price</th>
						<th className='text-center align-middle' style={{width: '150px'}}>Quantity</th>	
						<th className='text-center align-middle' style={{width: '140px'}}>Image</th>
						<th className='text-center align-middle' style={{width: '140px'}}>Subtotal</th>
						<th className='text-center align-middle' style={{width: '120px'}}>Action</th>
					</tr>
				</thead>
				
				{products}
				
			</Table> 
		</Container>
			:
		<Navigate to="/home" />


		)

};


// import {useEffect, useState, useContext } from 'react';
// import { Navigate, Link } from 'react-router-dom';
// import UserContext from '../UserContext';


// import { Table, Container, Form, Button } from 'react-bootstrap';
// import '../css/style.css'

// export default function Cart() {

// 	const [products, setProducts] = useState('');
// 	const [quantity, setQuantity] = useState();
// 	const { user } = useContext(UserContext);

	
// 	useEffect(()=> {

// 		fetch(`${process.env.REACT_APP_API_URL}/users/getCartDetails`,{
// 			method: 'GET',
// 			headers:{
// 				'Content-Type': 'application/json',
// 				Authorization: `Bearer ${localStorage.getItem('token')}`
// 			},
// 		})
// 		.then(res => res.json())
// 		.then(data =>{
 
// 			   if (data !== null) {

// 			 	  	setProducts(data.map(productData =>{
// 			   		return(
// 			   			<tbody key={productData._id}>
// 			   				<tr>
// 			   					<th className='ps-3 text-left align-middle' style={{width: '170px', height: '60px'}}>{productData.name}</th>
// 			   					<th className='text-center align-middle' style={{width: '90px'}}>{productData.price}</th>
// 			   					<th className='text-center align-middle'>
			   						// <Form.Group className="inputForm-Table">
			   						// 	<Button variant="danger" type="submit">
			   						// 	  +
			   						// 	</Button>
								    //    <Form.Control type="text" 	
								       	 	
								    //     />
								    //     <Button variant="danger" type="submit">
								    //    -
								  	//    </Button>
								    //  </Form.Group>
								     
// 			   					</th>
// 			   					<th className='text-center align-middle'>{productData.price * productData.quantity}</th>
// 			   					<th className='text-center align-middle'><img src={productData.imageLink} alt='products' style={{maxWidth: '100px'}}/></th>
// 			   					<th className='text-center align-middle'></th>
// 			   				</tr>
// 			   			</tbody>
// 			   			)
// 				   	}))

// 			   }else{

// 			   	return null
// 			   }
			
// 		})

// 	},[])


// 	return(

// 		(user.id !== null && user.isAdmin !== true) ?
// 		<Container className='mt-5'>
			
// 			<Table striped bordered hover size="sm" className='table-dark'>
// 				<thead className='table-dark'>
// 					<tr>
// 						<th className='text-center align-middle' style={{width: '170px', height: '60px'}}>Name</th>
// 						<th className='text-center align-middle' style={{width: '90px'}}>Price</th>
// 						<th className='text-center align-middle' style={{width: '150px'}}>Quantity</th>
// 						<th className='text-center align-middle' style={{width: '140px'}}>Subtotal</th>
// 						<th className='text-center align-middle' style={{width: '140px'}}>Image</th>
// 						<th className='text-center align-middle' style={{width: '120px'}}>Action</th>
// 					</tr>
// 				</thead>
				
// 				{products}
				
// 			</Table> 
// 		</Container>
// 			:
// 		<Navigate to="/home" />


// 		)

// };